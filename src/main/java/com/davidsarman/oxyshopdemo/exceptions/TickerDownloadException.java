package com.davidsarman.oxyshopdemo.exceptions;

public class TickerDownloadException extends Exception{
    public TickerDownloadException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public TickerDownloadException(String s) {
        super(s);
    }
}
