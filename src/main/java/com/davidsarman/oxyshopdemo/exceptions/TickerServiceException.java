package com.davidsarman.oxyshopdemo.exceptions;

public class TickerServiceException extends Exception {
    public TickerServiceException(Throwable throwable) {
        super(throwable);
    }

    public TickerServiceException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public TickerServiceException(String s) {
        super(s);
    }
}
