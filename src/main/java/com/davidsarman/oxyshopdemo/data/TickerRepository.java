package com.davidsarman.oxyshopdemo.data;

import com.davidsarman.oxyshopdemo.data.model.Ticker;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface TickerRepository extends JpaRepository<Ticker, Long> {
    Ticker findFirstByOrderByTimeDesc();

    List<Ticker> findTop2ByOrderByTimeDesc();

    List<Ticker> findByTimeAfterOrderByTimeAsc(LocalDateTime threshold);

    void deleteByTimeBefore(LocalDateTime threshold);
}
