package com.davidsarman.oxyshopdemo.data.model;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "TICKER")
public class Ticker {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Setter(AccessLevel.NONE)
    @EqualsAndHashCode.Include
    private Long id;

    @NotNull
    @Column(name = "RATE")
    private BigDecimal rate;

    @NotNull
    @Column(name = "TIME")
    private LocalDateTime time;

    public Ticker() {
    }

    public Ticker(@NotNull BigDecimal rate, @NotNull LocalDateTime time) {
        this.rate = rate;
        this.time = time;
    }
}
