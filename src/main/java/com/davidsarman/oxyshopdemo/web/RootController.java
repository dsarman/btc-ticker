package com.davidsarman.oxyshopdemo.web;

import com.davidsarman.oxyshopdemo.exceptions.TickerNotFoundException;
import com.davidsarman.oxyshopdemo.exceptions.TickerServiceException;
import com.davidsarman.oxyshopdemo.service.TickerService;
import com.davidsarman.oxyshopdemo.service.dtos.TickerDTO;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.connector.ClientAbortException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

@Controller
@Slf4j
public class RootController {
    private long rateEmitPause;
    private long emitTimeout;
    private TickerService tickerService;
    private ExecutorService executorService;
    private ObjectMapper objectMapper;

    public RootController(@Value("${sse.rate.emit.pause.ms}") long rateEmitPause,
                          @Value("${sse.emit.timeout.ms}") long emitTimeout,
                          TickerService tickerService,
                          ExecutorService executorService,
                          ObjectMapper objectMapper) {
        this.rateEmitPause = rateEmitPause;
        this.emitTimeout = emitTimeout;
        this.tickerService = tickerService;
        this.executorService = executorService;
        objectMapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
        this.objectMapper = objectMapper;
    }

    @GetMapping("/")
    public ModelAndView main() throws TickerServiceException, TickerNotFoundException, JsonProcessingException {
        ModelAndView modelAndView = new ModelAndView("home");
        TickerDTO currentTicker = tickerService.getCurrent();
        BigDecimal change = tickerService.getChange();
        List<TickerDTO> tickers = tickerService.getLastWeek();

        String chartDataJson = objectMapper.writeValueAsString(getChartData(tickers));
        modelAndView.addObject("currentTicker", currentTicker);
        modelAndView.addObject("change", change == null ? BigDecimal.ZERO : change);
        modelAndView.addObject("tickerChartData", chartDataJson);
        return modelAndView;
    }

    @GetMapping("/rate-sse")
    public SseEmitter emitRate() {
        SseEmitter emitter = new SseEmitter(emitTimeout);
        executorService.execute(() -> {
            long started = System.currentTimeMillis();
            boolean stop = false;
            TickerDTO lastSent = null;
            while (!stop) {
                try {
                    TickerDTO ticker = tickerService.getCurrent();
                    SseEmitter.SseEventBuilder event;
                    if (lastSent == ticker) {
                        event = SseEmitter.event().comment("KeepAlive");
                    } else {
                        event = SseEmitter.event()
                                .data(ticker.getTime().toString() + "," + ticker.getRate().toString())
                                .id(String.valueOf(ticker.hashCode()));
                    }

                    if (started + emitTimeout > System.currentTimeMillis()) {
                        emitter.send(event);
                        lastSent = ticker;
                        Thread.sleep(rateEmitPause);
                    } else {
                        emitter.complete();
                        stop = true;
                    }
                } catch (TickerServiceException | TickerNotFoundException e) {
                    log.error("Could not send rate as SSE event because of no data or service exception.", e);
                    emitter.completeWithError(e);
                    stop = true;
                } catch (ClientAbortException e) {
                    emitter.completeWithError(e);
                    stop = true;
                } catch (Exception e) {
                    log.warn("Rate SSE emitter failed with exception, completing it with error", e);
                    emitter.completeWithError(e);
                    stop = true;
                }
            }
        });
        return emitter;
    }

    private List<TickerChartData> getChartData(List<TickerDTO> tickers) {
        List<TickerChartData> data = new ArrayList<>();
        BigDecimal lastRate = null;
        for (TickerDTO ticker : tickers) {
            if (lastRate != null) {
                // Added to make straight line between the start and end of period without rate change
                data.add(new TickerChartData(ticker.getTime().minusSeconds(10), lastRate));
            }
            data.add(new TickerChartData(ticker.getTime(), ticker.getRate()));
            lastRate = ticker.getRate();
        }
        return data;
    }

    @Data
    @AllArgsConstructor
    private class TickerChartData {
        private LocalDateTime x;
        private BigDecimal y;
    }
}
