package com.davidsarman.oxyshopdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@EnableScheduling
@SpringBootApplication
public class OxyshopdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(OxyshopdemoApplication.class, args);
	}

	@Bean
	public ExecutorService getTaskExecutor() {
		return Executors.newCachedThreadPool();
	}
}
