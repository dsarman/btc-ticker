package com.davidsarman.oxyshopdemo.service;

import com.davidsarman.oxyshopdemo.data.TickerRepository;
import com.davidsarman.oxyshopdemo.data.model.Ticker;
import com.davidsarman.oxyshopdemo.exceptions.TickerNotFoundException;
import com.davidsarman.oxyshopdemo.exceptions.TickerServiceException;
import com.davidsarman.oxyshopdemo.service.dtos.TickerDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class TickerServiceImpl implements TickerService {
    private TickerRepository tickerRepository;
    private TickerDTO currentTicker = null;
    private BigDecimal change = null;
    private int retentionDays;

    public TickerServiceImpl(TickerRepository tickerRepository, @Value("${ticker.db.retention.days}") int retention) {
        this.tickerRepository = tickerRepository;
        this.retentionDays = retention;
    }

    @Override
    public TickerDTO getCurrent() throws TickerServiceException, TickerNotFoundException {
        if (currentTicker != null) {
            return currentTicker;
        }
        Ticker tickerEntry;
        try {
            tickerEntry = tickerRepository.findFirstByOrderByTimeDesc();
        } catch (DataAccessException e) {
            throw new TickerServiceException(e);
        }
        if (tickerEntry == null) {
            throw new TickerNotFoundException();
        }
        return toTickerDTO(tickerEntry);
    }

    @Override
    public BigDecimal getChange() {
        if (change == null) {
            List<Ticker> tickers = tickerRepository.findTop2ByOrderByTimeDesc();
            if (tickers.size() != 2) {
                log.warn("Did not get 2 results when trying to load last 2 ticker values, instead got '{}'", tickers);
            } else {
                change = tickers.get(0).getRate().subtract(tickers.get(1).getRate());
            }
        }
        return change;
    }

    @Override
    public void save(TickerDTO tickerDTO) throws TickerServiceException {
        if (currentTicker == null) {
            try {
                currentTicker = getCurrent();
            } catch (TickerNotFoundException e) {
                log.warn("Could not found ticker in db when no cached ticker is found.");
            }
        }
        if (currentTicker != null && currentTicker.getRate().equals(tickerDTO.getRate())) {
            log.info("Skipping saving of ticker info, since there was no change from last saved value");
            return;
        }
        Ticker tickerEntity = toTickerEntity(tickerDTO);
        try {
            tickerRepository.save(tickerEntity);
        } catch (DataAccessException e) {
            String msg = "Could not save ticker data '" + tickerDTO + "' because of exception at data layer.";
            throw new TickerServiceException(msg, e);
        }
        log.info("Successfully saved ticker '{}' into database", tickerEntity);
        if (currentTicker == null || currentTicker.getTime().isBefore(tickerDTO.getTime())) {
            if (currentTicker != null) {
                change = tickerDTO.getRate().subtract(currentTicker.getRate());
            }
            currentTicker = tickerDTO;
            log.debug("Cached new ticker data '{}'", tickerDTO);
        }
    }

    @Override
    public List<TickerDTO> getLastWeek() throws TickerServiceException {
        LocalDateTime weekAgo = LocalDateTime.now().minusDays(7);
        List<Ticker> tickers;
        try {
            tickers = tickerRepository.findByTimeAfterOrderByTimeAsc(weekAgo);
        } catch (DataAccessException e) {
            throw new TickerServiceException(e);
        }
        List<TickerDTO> result = new ArrayList<>();
        if (tickers == null || tickers.isEmpty()) {
            return result;
        }
        for (Ticker ticker : tickers) {
            result.add(toTickerDTO(ticker));
        }
        return result;
    }

    public void deleteBefore(LocalDateTime threshold) throws TickerServiceException {
        try {
            tickerRepository.deleteByTimeBefore(threshold);
        } catch (DataAccessException e) {
            throw new TickerServiceException(e);
        }
    }

    public void doCleanup(){
        LocalDateTime threshold = LocalDateTime.now().minusDays(retentionDays);
        try {
            deleteBefore(threshold);
        } catch (TickerServiceException e) {
            log.error("Could not do DB ticker data cleanup due to exception.", e);
        }
    }

    private TickerDTO toTickerDTO(Ticker ticker) {
        return new TickerDTO(ticker.getRate(), ticker.getTime());
    }

    private Ticker toTickerEntity(TickerDTO tickerDTO) {
        return new Ticker(tickerDTO.getRate(), tickerDTO.getTime());
    }
}
