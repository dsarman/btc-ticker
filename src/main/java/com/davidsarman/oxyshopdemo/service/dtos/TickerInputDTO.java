package com.davidsarman.oxyshopdemo.service.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TickerInputDTO {
    private boolean error;
    private String erorrMessage;
    private TickerDataDTO data;
}
