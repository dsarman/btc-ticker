package com.davidsarman.oxyshopdemo.service.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TickerDataDTO {
    private BigDecimal last;
    private BigDecimal high;
    private BigDecimal low;
    private BigDecimal amount;
    private BigDecimal bid;
    private BigDecimal ask;
    private BigDecimal change;
    private BigDecimal open;
    private long timestamp;
}
