package com.davidsarman.oxyshopdemo.service.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TickerDTO {
    private BigDecimal rate;
    private LocalDateTime time;
}
