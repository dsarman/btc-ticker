package com.davidsarman.oxyshopdemo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.ScheduledFuture;

@Component
@Slf4j
public class Scheduler {
    private TaskScheduler scheduler;
    private TickerCacher cacher;
    private TickerService service;
    private long tickerRefreshRate;
    private long tickerCleanupRate;

    public Scheduler(@Value("${ticker.caching.rate.ms}") long tickerRefreshRate,
                     @Value("${ticker.db.cleanup.period.ms}") long tickerCleanupRate,
                     TickerService service, TaskScheduler scheduler, TickerCacher cacher) {
        this.scheduler = scheduler;
        this.tickerRefreshRate = tickerRefreshRate;
        this.tickerCleanupRate = tickerCleanupRate;
        this.cacher = cacher;
        this.service = service;
    }

    @PostConstruct
    public void setTickerRefreshSchedule() {
        log.debug("Running TickerCacher initial schedule set up with rate of '{}' ms.", tickerRefreshRate);
        ScheduledFuture future = scheduler.scheduleAtFixedRate(cacher::cacheTickerData, tickerRefreshRate);
        log.info("Successfully set up scheduled job to refresh ticker data '{}'", future);
    }

    @PostConstruct
    public void setTickerCleanupSchedule() {
        log.debug("Running Ticker db cleanup initial schedule set up with rate of '{}' ms.", tickerCleanupRate);
        ScheduledFuture future = scheduler.scheduleAtFixedRate(service::doCleanup, tickerCleanupRate);
        log.info("Successfully set up scheduled job to refresh ticker data '{}'", future);
    }
}
