package com.davidsarman.oxyshopdemo.service;

import com.davidsarman.oxyshopdemo.exceptions.TickerNotFoundException;
import com.davidsarman.oxyshopdemo.exceptions.TickerServiceException;
import com.davidsarman.oxyshopdemo.service.dtos.TickerDTO;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public interface TickerService {
    /**
     * Returns the most recent Ticker data.
     * If this data is not cached in the service, database call will be made.
     */
    TickerDTO getCurrent() throws TickerServiceException, TickerNotFoundException;

    /**
     * Returns result of current rate - rate before current one.
     * Negative number means the rate is dropping, positive that it is increasing.
     */
    BigDecimal getChange();

    /**
     * Returns list of all retained Ticker values over the last week.
     */
    List<TickerDTO> getLastWeek() throws TickerServiceException;

    /**
     * Saves the given ticker into database.
     * Also caches the value in service if it is newer than the currently cached one.
     */
    void save(TickerDTO tickerDTO) throws TickerServiceException;

    /**
     * Deletes all ticker records before the supplied date.
     */
    void deleteBefore(LocalDateTime threshold) throws TickerServiceException;

    /**
     * Deletes all ticker data older than retention period from database.
     */
    void doCleanup();
}
