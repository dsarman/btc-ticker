package com.davidsarman.oxyshopdemo.service;

import com.davidsarman.oxyshopdemo.exceptions.TickerDownloadException;
import com.davidsarman.oxyshopdemo.exceptions.TickerServiceException;
import com.davidsarman.oxyshopdemo.service.dtos.TickerDTO;
import com.davidsarman.oxyshopdemo.service.dtos.TickerDataDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * Class that upon Spring context initialization schedules a Ticker value caching job, running periodically with rate
 * specified by ticker.caching.rate.ms property.
 */
@Service
@Slf4j
public class TickerCacherImpl implements TickerCacher {
    private TickerDownloader downloader;
    private TickerService tickerService;

    public TickerCacherImpl(TickerDownloader downloader, TickerService tickerService) {
        this.downloader = downloader;
        this.tickerService = tickerService;
    }

    @Override
    public void cacheTickerData() {
        log.debug("Starting ticker data refresh job");
        TickerDataDTO tickerData;
        try {
            tickerData = downloader.fetchCurrent();
        } catch (TickerDownloadException e) {
            log.debug("Could not download current ticker data, skipping this cache refresh job", e);
            return;
        }

        Instant timestamp = Instant.ofEpochSecond(tickerData.getTimestamp());
        LocalDateTime time = LocalDateTime.ofInstant(timestamp, ZoneId.of("UTC"));

        TickerDTO tickerDTO = new TickerDTO(tickerData.getLast().setScale(2, BigDecimal.ROUND_HALF_UP), time);
        try {
            tickerService.save(tickerDTO);
        } catch (TickerServiceException e) {
            log.error("Could not save ticker data '{}' because of exception", tickerDTO);
        }
    }
}
