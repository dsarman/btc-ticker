package com.davidsarman.oxyshopdemo.service;

import com.davidsarman.oxyshopdemo.exceptions.TickerDownloadException;
import com.davidsarman.oxyshopdemo.service.dtos.TickerDataDTO;

public interface TickerDownloader {
    /**
     * Retrieves current price Ticker for BTC - CZK currency pair from the api
     * @return DTO object with current ticker information.
     */
    TickerDataDTO fetchCurrent() throws TickerDownloadException;
}
