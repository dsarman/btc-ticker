package com.davidsarman.oxyshopdemo.service;

public interface TickerCacher {
    /**
     * Downloads current Ticker data, and saves them into the database.
     */
    void cacheTickerData();
}
