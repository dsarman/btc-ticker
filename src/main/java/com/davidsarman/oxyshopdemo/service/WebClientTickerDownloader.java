package com.davidsarman.oxyshopdemo.service;

import com.davidsarman.oxyshopdemo.exceptions.TickerDownloadException;
import com.davidsarman.oxyshopdemo.service.dtos.TickerInputDTO;
import com.davidsarman.oxyshopdemo.service.dtos.TickerDataDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;

@Service
@Slf4j
public class WebClientTickerDownloader implements TickerDownloader {
    private String apiUrl;
    private WebClient webClient;

    public WebClientTickerDownloader(@Value("${ticker.api.url}") String apiUrl) {
        this.apiUrl = apiUrl;
        webClient = WebClient.create(apiUrl);
    }

    @Override
    public TickerDataDTO fetchCurrent() throws TickerDownloadException {
        log.debug("Fetching ticker data from '{}'", apiUrl);
        TickerInputDTO ticker;
        try {
            ticker = webClient.get().retrieve().bodyToMono(TickerInputDTO.class).block();
        } catch (WebClientException e) { // In case of 4xx or 5xx HTTP return codes
            throw new TickerDownloadException("Could not fetch current ticker data from " + apiUrl, e);
        }

        if (ticker == null) {
            throw new TickerDownloadException("Data returned when fetching ticker from '" + apiUrl + "' is null.");
        } else if (ticker.isError()) {
            throw new TickerDownloadException("Download request to '" + apiUrl + "' for ticker data succeeded," +
                    " but the returned data indicates error with message " + ticker.getErorrMessage());
        }
        log.info("Successfully retrieved ticker data '{}' from '{}'", ticker, apiUrl);

        return ticker.getData();
    }
}
