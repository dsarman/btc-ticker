var firstEvent = true;
var lastEventId;

if (!!window.EventSource) {
  var source = new EventSource('/rate-sse');

  source.addEventListener('message', function (evt) {
    console.debug(evt);
    if (lastEventId === evt.lastEventId) {
      return;
    } else {
      lastEventId = evt.lastEventId;
    }

    var splitData = evt.data.split(',');
    var rate = parseFloat(splitData[1]);

    if (firstEvent) {
      firstEvent = false;
    } else {
      setRateAndChange(rate);
      var timestamp = splitData[0];
      updateChart(timestamp, rate);
    }
    currentRate = rate;
  });
} else {
  alert('Use browser with SSE support to see updates to the rate without refreshing your window.');
}

function setRateAndChange (rate) {
  $('#current-rate').text(rate);
  var difference = (rate - currentRate).toFixed(2);
  $('#rate-change-value').text(difference < 0 ? difference : '+' + difference);
  if (difference === 0) {
    $('#rate-change-text').attr('class', 'hidden');
    $('#rate-up-img').attr('class', 'hidden');
    $('#rate-down-img').attr('class', 'hidden');
  } else if (difference > 0) {
    $('#rate-change-text').attr('class', '');
    $('#rate-up-img').attr('class', '');
    $('#rate-down-img').attr('class', 'hidden');
  } else {
    $('#rate-change-text').attr('class', '');
    $('#rate-up-img').attr('class', 'hidden');
    $('#rate-down-img').attr('class', '');
  }
}

function updateChart (timestamp, rate) {
  var parsedTimestamp = moment.utc(timestamp);
  var oldTimestamp = parsedTimestamp.subtract(10, 'second').format('YYYY-MM-DDTHH:mm:ss');
  rateChart.data.datasets.forEach(function (dataset) {
    dataset.data.push({x: oldTimestamp, y: currentRate});
    dataset.data.push({x: timestamp, y: rate});
  });
  rateChart.update();
}