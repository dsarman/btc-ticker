package com.davidsarman.oxyshopdemo;

import com.davidsarman.oxyshopdemo.service.dtos.TickerInputDTO;
import com.davidsarman.oxyshopdemo.service.dtos.TickerDataDTO;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.Instant;

public class TestUtils {
    @Getter
    private static Instant timestamp = Instant.ofEpochSecond(1552336046);

    public static TickerInputDTO getValidTestTicker() {
        return new TickerInputDTO(false, null, new TickerDataDTO(
                BigDecimal.valueOf(88090), BigDecimal.valueOf(89405.42), BigDecimal.valueOf(87154.28),
                BigDecimal.valueOf(45.36015373), BigDecimal.valueOf(88000), BigDecimal.valueOf(88112.77),
                BigDecimal.valueOf(-0.74), BigDecimal.valueOf(88749.62), timestamp.getEpochSecond()
        ));
    }


}
