package com.davidsarman.oxyshopdemo.data;

import com.davidsarman.oxyshopdemo.data.model.Ticker;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TickerRepositoryTest {

    @Autowired
    TickerRepository repository;

    @Test
    public void findFirstByOrderByTime() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime before = now.minusMinutes(10);
        Ticker tickerNow = new Ticker(BigDecimal.TEN, now);
        Ticker tickerBefore = new Ticker(BigDecimal.ZERO, before);
        repository.saveAll(Arrays.asList(tickerBefore, tickerNow));
        Ticker current = repository.findFirstByOrderByTimeDesc();
        assertEquals(tickerNow, current);
    }

    @Test
    public void findByTimeAfterAndOrderByTime() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime middle = now.minusMinutes(10);
        LocalDateTime before = now.minusMinutes(20);
        Ticker tickerNow = new Ticker(BigDecimal.TEN, now);
        Ticker tickerMiddle = new Ticker(BigDecimal.ZERO, middle);
        Ticker tickerBefore = new Ticker(BigDecimal.ZERO, before);
        repository.saveAll(Arrays.asList(tickerNow, tickerBefore, tickerMiddle));

        List<Ticker> tickersYoungerThan15Minutes = repository.findByTimeAfterOrderByTimeAsc(now.minusMinutes(15));
        assertEquals(2, tickersYoungerThan15Minutes.size());
        assertTrue(tickersYoungerThan15Minutes.contains(tickerNow));
        assertTrue(tickersYoungerThan15Minutes.contains(tickerMiddle));
    }
}