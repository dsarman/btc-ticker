package com.davidsarman.oxyshopdemo.service;

import com.davidsarman.oxyshopdemo.TestUtils;
import com.davidsarman.oxyshopdemo.exceptions.TickerDownloadException;
import com.davidsarman.oxyshopdemo.exceptions.TickerServiceException;
import com.davidsarman.oxyshopdemo.service.dtos.TickerDTO;
import com.davidsarman.oxyshopdemo.service.dtos.TickerInputDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TickerCacherImplTest {

    @Mock
    private TickerDownloader downloader;

    @Mock
    private TickerService service;

    private TickerCacherImpl tickerCacher;
    private TickerInputDTO tickerInputDTO;

    @Before
    public void setUp() {
        tickerCacher = new TickerCacherImpl(downloader, service);
        tickerInputDTO = TestUtils.getValidTestTicker();
    }

    @Test
    public void testSuccessfulDownload() throws TickerDownloadException, TickerServiceException {
        when(downloader.fetchCurrent()).thenReturn(tickerInputDTO.getData());
        tickerCacher.cacheTickerData();
        TickerDTO tickerDTO = new TickerDTO(tickerInputDTO.getData().getLast().setScale(2, BigDecimal.ROUND_HALF_UP),
                LocalDateTime.ofInstant(TestUtils.getTimestamp(), ZoneId.of("UTC")));

        verify(downloader, times(1)).fetchCurrent();
        verify(service, times(1)).save(tickerDTO);
        verifyNoMoreInteractions(downloader, service);
    }

    @Test
    public void testFailedDownload() throws TickerDownloadException {
        when(downloader.fetchCurrent()).thenThrow(new TickerDownloadException("Test exception"));
        tickerCacher.cacheTickerData();
        verify(downloader, times(1)).fetchCurrent();
        verifyNoMoreInteractions(downloader, service);
    }

    @Test
    public void testFailedSave() throws TickerDownloadException, TickerServiceException {
        when(downloader.fetchCurrent()).thenReturn(tickerInputDTO.getData());
        doThrow(new TickerServiceException("Test exception")).when(service).save(any());
        tickerCacher.cacheTickerData();
        verify(downloader, times(1)).fetchCurrent();
        verify(service, times(1)).save(any(TickerDTO.class));
        verifyNoMoreInteractions(downloader, service);
    }
}