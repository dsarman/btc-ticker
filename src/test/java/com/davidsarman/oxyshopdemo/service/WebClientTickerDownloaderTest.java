package com.davidsarman.oxyshopdemo.service;

import com.davidsarman.oxyshopdemo.TestUtils;
import com.davidsarman.oxyshopdemo.exceptions.TickerDownloadException;
import com.davidsarman.oxyshopdemo.service.dtos.TickerInputDTO;
import com.davidsarman.oxyshopdemo.service.dtos.TickerDataDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.*;

public class WebClientTickerDownloaderTest {
    private static final int MOCKED_API_PORT = 8089;
    private static final String MOCKED_API_URL = "localhost:" + MOCKED_API_PORT + "/ticker";
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Rule
    public WireMockRule wireMockRule = new WireMockRule(MOCKED_API_PORT);
    private ObjectMapper mapper = new ObjectMapper();
    private TickerInputDTO defaultTicker;

    @Before
    public void setUp() {
        defaultTicker = TestUtils.getValidTestTicker();
    }

    @Test
    public void testCorrect() throws Exception {
        TickerDownloader downloader = new WebClientTickerDownloader(MOCKED_API_URL);
        stubFor(get(urlEqualTo("/ticker"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("content-type","application/json;charset=UTF-8")
                        .withBody(mapper.writeValueAsString(defaultTicker))));
        TickerDataDTO result = downloader.fetchCurrent();
        assertCorrectTicker(result);
    }

    @Test
    public void testWebClientException() throws Exception {
        TickerDownloader downloader = new WebClientTickerDownloader(MOCKED_API_URL);
        stubFor(get(urlEqualTo("/ticker"))
                .willReturn(aResponse()
                        .withStatus(404)));
        thrown.expect(TickerDownloadException.class);
        thrown.expectMessage("Could not fetch current ticker data from " + MOCKED_API_URL);
        downloader.fetchCurrent();
    }

    @Test
    public void testNullResultBody() throws Exception {
        TickerDownloader downloader = new WebClientTickerDownloader(MOCKED_API_URL);
        stubFor(get(urlEqualTo("/ticker"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("content-type","application/json;charset=UTF-8")
                        .withBody("")));
        thrown.expect(TickerDownloadException.class);
        thrown.expectMessage("Data returned when fetching ticker from '" + MOCKED_API_URL + "' is null.") ;
        downloader.fetchCurrent();
    }

    @Test
    public void testResultWithErrorData() throws Exception {
        TickerDownloader downloader = new WebClientTickerDownloader(MOCKED_API_URL);
        defaultTicker.setError(true);
        String errorMessage = "Testing error message that will be visible in the exception";
        defaultTicker.setErorrMessage(errorMessage);
        stubFor(get(urlEqualTo("/ticker"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("content-type","application/json;charset=UTF-8")
                        .withBody(mapper.writeValueAsString(defaultTicker))));
        thrown.expect(TickerDownloadException.class);
        thrown.expectMessage(errorMessage) ;
        downloader.fetchCurrent();
    }

    @Test
    public void testFetchCurrentLive() throws Exception {
        TickerDownloader downloader = new WebClientTickerDownloader("https://coinmate.io/api/ticker?currencyPair=BTC_CZK");
        TickerDataDTO result = downloader.fetchCurrent();
        assertCorrectTicker(result);
    }

    private void assertCorrectTicker(TickerDataDTO ticker) {
        assertNotNull(ticker);
        assertNotNull(ticker);
        assertNotNull(ticker.getLast());
        assertNotNull(ticker.getChange());
        assertNotEquals(ticker.getTimestamp(), 0);
    }
}