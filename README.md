# Bitcoin ticker for CZK/BTC rate
A sample application that fetches current rate from external REST API, stores the changes in values into the database, 
and displays the current rate, the amount of change against previous measurement, and a graphs historical data 7 days
into the past.

# Technologies used
Spring Boot application using Hibernate to persist the data, Thymeleaf templates to render the frontend, and Server-sent
events to update the web page without reload. The app is set up with Gitlab CI/CD that runs unit tests and auto deploys
to Heroku.

# Next steps
- [ ] Fix/Get rid of the misleading Tomcat exception:
  ```
  java.lang.IllegalStateException: Calling [asyncError()] is not valid for a request with Async state [MUST_DISPATCH]
  ```
  see https://github.com/spring-projects/spring-boot/issues/15057 for more info.
 - [ ] Better test coverage (for example the `TickerServiceImpl` class ).
    - Add Spring MVC tests
 - [ ] Handle the case where the client misses some SSE events which in turn do net get displayed in the graph.
    - Possibly by sending sequential IDs, we could detect missing events and fetch them manually.
 - [ ] Fine tune the SSE keepAlive, timeout and other settings, as Heroku logs sometime show a connection timeout occur
   when it should not.
 - ...
